package catalog

import "fmt"

func processGitLabTeams(teamsYAML GitLabTeamsYAML) (*GitLabTeams, error) {
	result := &GitLabTeams{
		GitLabTeamsYAML: teamsYAML,
	}

	teams := make([]GitLabTeam, len(result.GitLabTeamsYAML.GitLabTeams))
	for k, v := range result.GitLabTeamsYAML.GitLabTeams {
		team := GitLabTeam{v}
		teams[k] = team
	}
	result.GitLabTeams = teams
	return result, nil
}

func process(catalogYAML ServiceCatalogYAML) (*ServiceCatalog, error) {
	result := &ServiceCatalog{
		ServiceCatalogYAML: catalogYAML,
	}

	err := processTeams(result)
	if err != nil {
		return nil, err
	}

	err = processTiers(result)
	if err != nil {
		return nil, err
	}

	err = processServices(result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func processTeams(catalog *ServiceCatalog) error {
	teams := make([]Team, len(catalog.ServiceCatalogYAML.Teams))
	for k, v := range catalog.ServiceCatalogYAML.Teams {
		team := Team{v}
		teams[k] = team
	}
	catalog.Teams = teams
	return nil
}

func processTiers(catalog *ServiceCatalog) error {
	tiers := make([]Tier, len(catalog.ServiceCatalogYAML.Tiers))
	for k, v := range catalog.ServiceCatalogYAML.Tiers {
		tier := Tier{v}
		tiers[k] = tier
	}
	catalog.Tiers = tiers
	return nil
}

func processServices(catalog *ServiceCatalog) error {
	services := make([]Service, len(catalog.ServiceCatalogYAML.Services))
	teamMap := catalog.teamMap()
	tierMap := catalog.tierMap()

	for k, v := range catalog.ServiceCatalogYAML.Services {
		teams, err := pickTeams(v.Teams, teamMap)
		if err != nil {
			return err
		}
		tier, ok := tierMap[v.Tier]
		if !ok {
			return fmt.Errorf("Unable to find tier %v", v.Tier)
		}
		service := Service{
			ServiceYAML: v,
			Teams:       teams,
			Tier:        tier,
		}
		services[k] = service
	}
	catalog.Services = services
	return nil
}

func pickTeams(teamNames []string, teamMap map[string]Team) ([]Team, error) {
	result := make([]Team, len(teamNames))
	for k, v := range teamNames {
		team, ok := teamMap[v]
		if !ok {
			return nil, fmt.Errorf("Unable to find team %v", v)
		}
		result[k] = team
	}
	return result, nil
}
